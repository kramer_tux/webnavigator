#include "fenetreprincipale.h"
#include "ui_fenetreprincipale.h"
#include <QWidget>
#include <QWebEngineView>

fenetrePrincipale::fenetrePrincipale(QString adresse, int valueProgressBar, QWidget *parent) :QMainWindow(parent),
    ui(new Ui::fenetrePrincipale),
    m_valueProgressBar(valueProgressBar),
    m_adresse(adresse)
{
    m_onglet = new QTabWidget(this);

    m_barreDeRecherche = new QLineEdit();
    ui->setupUi(this);



    m_onglet->addTab(creationNouvellePageWeb("http://www.google.com"), "www.google.com");
    ui->centralWidget->setLayout(ui->verticalLayout);
    setCentralWidget(m_onglet);

    ui->mainToolBar->addWidget(m_barreDeRecherche);
    ui->statusBar->addWidget(ui->progressBar, 1);

    ui->actionnouvel_onglet->setShortcut(Qt::CTRL + Qt::Key_N);
    connect(ui->actionnouvel_onglet, SIGNAL(triggered()), SLOT(nouvelOnglet()));
    connect(m_barreDeRecherche, SIGNAL(returnPressed()), this, SLOT(addressEntered()));
    connect(m_onglet, SIGNAL(currentChanged(int)), this, SLOT(changeIndex(int)));
}
void fenetrePrincipale::changeIndex(int index)
{
    qDebug() << " index : " << index << endl;
}

void fenetrePrincipale::changedUrl(const QUrl &myUrl)
{
    m_adresse = myUrl.toString();
    m_barreDeRecherche->setText(m_adresse);
    qDebug() << m_adresse << endl;
    m_onglet->setTabText(m_onglet->currentIndex(), m_adresse);
}

QWebEngineView* fenetrePrincipale:: pageActuelle()
{
    return m_onglet->currentWidget()->findChild<QWebEngineView*>();
}

void fenetrePrincipale::updateProgressBar(int progress)
{
    m_valueProgressBar = progress;
    ui->progressBar->setValue(m_valueProgressBar);
}

void fenetrePrincipale::nouvelOnglet()
{
    int index = m_onglet->addTab(creationNouvellePageWeb("http://www.google.com"), "www.google.com");
    m_onglet->setCurrentIndex(index);
    m_onglet->setMovable(true);
}

void fenetrePrincipale::addressEntered()
{
    QString data;

    if(!m_barreDeRecherche->text().isEmpty())
    {
        data = "http://" + m_barreDeRecherche->text() + ".com";
        pageActuelle()->setUrl(data);
    }
}

QWidget* fenetrePrincipale:: creationNouvellePageWeb(QString adresse) // QWidget* lwidget)
{
    QWidget* widget = new QWidget;
    QWebEngineView* newPage = new QWebEngineView;
    if(!adresse.isEmpty())
    {
        newPage->load(QUrl(adresse));
    }
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setContentsMargins(0,0,0,0);
    layout->addWidget(newPage);
    widget->setLayout(layout);

    connect(newPage, SIGNAL(loadProgress(int)), SLOT(updateProgressBar(int)));
    connect(newPage, SIGNAL(urlChanged(const QUrl&)), this, SLOT(changedUrl(const QUrl&)));

    return widget;
}

fenetrePrincipale::~fenetrePrincipale()
{
    delete ui;
}

void fenetrePrincipale::on_actionNext_triggered()
{
    pageActuelle()->forward();
}

void fenetrePrincipale::on_actionPr_c_dent_triggered()
{
    pageActuelle()->back();
}

void fenetrePrincipale::on_actionArret_triggered()
{
    pageActuelle()->stop();
}

void fenetrePrincipale::on_actionhome_triggered()
{
    pageActuelle()->load(QUrl("http://www.google.fr"));
}
