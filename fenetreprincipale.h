#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H
#include <QMainWindow>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QtWebEngineWidgets>
#include <QUrl>
#include <QTabWidget>


namespace Ui {
class fenetrePrincipale;
}

class fenetrePrincipale : public QMainWindow
{
    Q_OBJECT

public:
    explicit fenetrePrincipale(QString adresse, int valueProgressBar, QWidget *parent = nullptr);
    QWidget* creationNouvellePageWeb(QString adresse);
    ~fenetrePrincipale();

public slots :
    void addressEntered();
    void updateProgressBar(int progress);
    void nouvelOnglet();
    void changeIndex(int index);
    void changedUrl(const QUrl& myUrl);
    QWebEngineView* pageActuelle();

private slots:
    void on_actionNext_triggered();

    void on_actionPr_c_dent_triggered();

    void on_actionArret_triggered();

    void on_actionhome_triggered();

private:
    Ui::fenetrePrincipale *ui;
    QLineEdit* m_barreDeRecherche;
    QUrl m_url;
    QWidget* m_widget;
    int m_valueProgressBar;
    QTabWidget* m_onglet;
    QString m_adresse;
};

#endif // FENETREPRINCIPALE_H
